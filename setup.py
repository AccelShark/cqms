from distutils.core import setup

setup(
    name='CQMS',
    version='0.2.0',
    packages=['cqms', ],
    scripts=['bin/cqms'],
    license='ISC license',
    long_description=open('README.txt').read(),
)
