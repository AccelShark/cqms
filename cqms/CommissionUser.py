class CommissionUser(object):
    """A commission user entry.
    This contains:
    - a name entry, representing the user handle.
    - a type entry, representing the `CommissionType` name for lookup.
    - a miscellaneous data entry."""

    def __init__(self, name, type, data):
        super(CommissionUser, self).__init__()
        self.name = name
        self.type = type
        self.data = data
