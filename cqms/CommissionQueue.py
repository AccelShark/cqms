import csv
import math
import cqms
import cqms.CommissionType as commt
import cqms.CommissionUser as commu


class FullWarning(Warning):
    """If a commission queue database is full of entries, a `FullWarning`
    is raised."""

    def __init__(self, arg):
        super(FullWarning, self).__init__()


class StaleWarning(Warning):
    """If a commission queue database is stale on the minor semver, a
    `StaleWarning` is raised."""

    def __init__(self, arg):
        super(StaleWarning, self).__init__()


class StaleError(StaleWarning, Exception):
    """If a commission queue database is stale on the major semver, a
    `StaleError` is raised."""

    def __init__(self, arg):
        super(StaleError, self).__init__()


class CommissionQueue(object):
    """A commission queue representation convertable to and from CSV."""

    def __init__(self):
        super(CommissionQueue, self).__init__()
        self.types = {}
        self.queue = []
        self.users = {}
        self.version = [0, 2, 0]
        self.calls = {
            'TYPE': self.modify_type,
            'USER': self.modify_user,
            'QENT': lambda n, k: self.queue.insert(n, k),
            'VERS': self.assert_vers
        }

        try:
            self.read_csv()
        except FileNotFoundError:
            print("Can't open commission queue, but that's okay...?")
            with open(cqms.data, 'w') as o:
                oc = csv.writer(o)
                oc.writerow(['VERS', *self.version])
            self.read_csv()

    def assert_vers(self, major, minor, patch):
        """Assert that this database version is supported."""
        if self.version[0] > int(major):
            print("Please migrate your commission queue by hand or delete it.")
            raise StaleError(
                "v{} > CSV table v{}".format(self.version[0], major))
        elif self.version[1] > int(minor):
            print("v{} <= CSV table v{}".format(self.version[0], major))
            print("You may have to migrate your commission queue CSV.")
            raise StaleWarning(
                "v_.{} > CSV table v_.{}".format(self.version[1], minor))
        else:
            print("v_.{} <= CSV table v_.{}".format(self.version[1], minor))
            print("Your commission queue CSV is up to date.")

    def modify_type(self, name, desc):
        """Modify a `CommissionType`."""
        name = name[0:64]
        self.types[name] = commt.CommissionType(name, desc)

    def modify_user(self, name, typed, data):
        """Modify a `CommissionUser`."""
        name = name[0:64]
        data = data[0:2048]
        typed = typed[0:64]
        typespec = None
        if typed in self.types:
            typespec = self.types[typed]
        else:
            return  # TODO: Error here?

        print("Changing user ({}, {}): {}".format(name, typed, data))
        if name not in self.users and len(self.users) >= cqms.userlimit:
            raise FullWarning(
                "Commission queue full ({})".format(cqms.userlimit))
        else:
            self.users[name] = commu.CommissionUser(
                name, typespec, data)

    def read_csv(self):
        """Try to read the CQMS CSV data."""
        self.types.clear()
        self.queue.clear()
        self.users.clear()
        with open(cqms.data, 'r') as i:
            ic = csv.reader(i)
            for icl in ic:
                try:
                    self.calls[icl[0]](*icl[1:])
                except StaleError:
                    return

    def write_csv(self):
        """Write the CQMS CSV data."""
        with open(cqms.data, 'w') as i:
            ic = csv.writer(i)
            ic.writerow(['VERS', *self.version])
            for k, v in self.types:
                ic.writerow(['TYPE', k, *v])
            for k, v in self.users:
                ic.writerow(['USER', k, *v])
            for n, k in enumerate(self.queue):
                ic.writerow(['QENT', n, k])

    def front(self):
        """Return the front `CommissionUser`.
        - If there is nothing in the queue, then return nothing."""
        cand = None
        if len(self.queue) > 0:
            cand = self.users[self.queue[0]]
        return cand

    def random(self):
        """Return a random `CommissionUser`.
         - If there is nothing in the queue, then return nothing."""
        cand = None
        if len(self.queue) > 0:
            cand = self.users[self.queue[int(math.random(0, len(self.queue)))]]
        return cand

    def pick(self, slot):
        """Return a `CommissionUser` at the designated slot.
         - If there is nothing in that slot, then return nothing."""
        cand = None
        if slot < len(self.queue):
            cand = self.users[self.queue[slot]]
        return cand
