class CommissionType(object):
    """A commission type entry.
    This contains:
    - a name entry, for lookup.
    - a description entry, describing this commission type's nature."""

    def __init__(self, name, desc):
        super(CommissionType, self).__init__()
        self.name = name
        self.desc = desc
